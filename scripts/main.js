/*
	Copyright 2013 - P_W999
	Licensed under The Code Project Open License (CPOL) 1.02 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

       http://www.codeproject.com/info/cpol10.aspx
	   
	Author: Phillip (p_w999)
	Blog: http://pw999.wordpress.com
	Contact: p_w999@ymail.com
	Twitter: @P_W999
	
	Donations are always welcome:
	- BTC: 1D1aGcfQNLS3D6bxghSVoKBnAq3pQqW8FW
	- LTC: LKu6G3BBmuu13ZiLwrbLxh7dwiJMFF9np7
*/

/**
*	Performs a synchronous GET on the given URL.<br />
*	This method will automatically fill in the fields in the main gadget window.
*	@param url the URL to GET
*	@param suffix a suffix that will be added to the ID of the elements used in the main gadget window.
*	@param type	the type of coin (currently supports LTC/BTC)
*	@return a miners object
*	
*/
function doAjaxCall(url, suffix, type) {
	var returnValue;
	var xmlHttp;
	try
	{
		/* Firefox, Opera 8.0+, Safari */
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		/* newer IE */
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
		/* older IE */
			try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser is old and does not have AJAX support!");
				return false;
			}
		}
	}
	xmlHttp.onreadystatechange=function()
		{
			if(xmlHttp.readyState==4)
			{
				try {
					returnValue = handleJSON(xmlHttp.responseText, suffix, type, url);
				} catch (err) {
					$('#debug').text(err.message);
				}
			} else  {
				$('#info').text('Working ...');
			}
		}
	//force a timestamp in the URL to ensure MSIE doesn't cache the HTTP GET calls :( 
	if (/\?/.test(url)) {
		xmlHttp.open("GET", url + '&timestamp=' + ((new Date()).getTime()) ,false);
	} else {
		xmlHttp.open("GET", url + '?timestamp=' + ((new Date()).getTime()) ,false);
	}
	
	xmlHttp.send(null);
	return returnValue;
}

/**
*	A proxy to the handleJSON_{TYPE} methods. Makes it easy to support other formats.
*	@param jsonString the JSON as a String
*	@param suffix the suffix for the HTML IDs
*	@param type the type of coin (BTC or LTC)
*	@param url the url that was called
*	@return a miners object
*/
function handleJSON(jsonString, suffix, type, url) {
	if (type == 'LTC') {
		return handleJSON_LTC(jsonString, suffix, type, url);
	} else {
		return handleJSON_BTC(jsonString, suffix, type, url);
	}
}

/**
*	Handles JSON from LTC mining pool
*/
function handleJSON_LTC(jsonString, suffix, type, url) {
	var json = jQuery.parseJSON(jsonString);
	$('#pool' + suffix).text(urlToName(url));
	$('#rewards' + suffix).text(json.confirmed_rewards + ' ' + type);
	$('#roundEstimate' + suffix).text(json.round_estimate + ' ' + type);
	$('#totalHashrate' + suffix).text(json.total_hashrate + ' kh/s');
	$('#workers' + suffix).empty();
	jQuery.each(json.workers, function(key, value) {
		if (value.alive==1) {
			$('#workers' + suffix).append('<li>' + key + ' - ' + value.hashrate + ' kh/s</li>');
		} else {
			$('#workers' + suffix).append('<li>' + key + ' - ' + 'offline' + '</li>');
		}
		
	});
	$('#info').text(moment().format('MMMM Do YYYY, h:mm:ss a'));
	return jsonToObject_LTC(jsonString, type);
}

/**
*	Handles JSON from a BTC mining pool
*/
function handleJSON_BTC(jsonString, suffix, type, url) {
	var json = jQuery.parseJSON(jsonString);
	$('#pool' + suffix).text(urlToName(url));
	$('#rewards' + suffix).text(json.confirmed_reward + ' ' + type);
	$('#roundEstimate' + suffix).text(json.estimated_reward + ' ' + type);
	$('#totalHashrate' + suffix).text(json.hashrate + ' Mh/s');
	$('#workers' + suffix).empty();
	jQuery.each(json.workers, function(key, value) {
		if (value.alive==1) {
			$('#workers' + suffix).append('<li>' + key + ' - ' + value.hashrate + ' Mh/s</li>');
		} else {
			$('#workers' + suffix).append('<li>' + key + ' - ' + 'offline' + '</li>');
		}
		
	});
	$('#info').text(moment().format('MMMM Do YYYY, h:mm:ss a'));
	return jsonToObject_BTC(jsonString, type);
}

/**
*	Converst a URL to a mining pool name. <br />
*	For example: 'http://www.miningpool.com/json?keeeeeeeyyyyyyyyy' will return 'miningpool.com' as name.
*	@param the url to the mining pool
*	@return the domain name of the pool
*/
function urlToName(url) {
	var sp = url.split('/')[2];
	if ((/^www/).test(sp)) {
		return sp.substring(4);
	} else {
		return sp;
	}
}

/**
*	Converts the LTC JSON string to a miners object
*/
function jsonToObject_LTC(jsonString, type) {
	var json = jQuery.parseJSON(jsonString);
	return new miners(json.confirmed_rewards, json.round_estimate, json.total_hashrate, type, json.workers);
}

/**
*	Converts the BTC JSON string to a miners object
*/
function jsonToObject_BTC(jsonString, type) {
	var json = jQuery.parseJSON(jsonString);
	return new miners(json.confirmed_rewards, json.round_estimate, json.total_hashrate, type, json.workers);
}

/**
*	Creates a miners object
*/
function miners(confirmedRewards, estimateRewards, hashRate, type, workers) {
	this.confirmedRewards = confirmedRewards;
	this.estimateRewards = estimateRewards;
	this.hashRate = hashRate;
	this.type = type;
	var i = 0;
	jQuery.each(workers, function() {	i = i + 1;	});
	this.workers = i;
}

/**
*	Mother of all functions.<br />
*	Is responsible for everything that is shown on screen.
*/
function init() {
	try {
		// Number of pools the user configured
		var count = System.Gadget.Settings.readString('count');
		// ensure a default value exists
		if (System.Gadget.Settings.readString('bgColor') == '') {
			System.Gadget.Settings.writeString('bgColor', 'black');
		}
		if (System.Gadget.Settings.readString('fgColor') == '') {
			System.Gadget.Settings.writeString('fgColor', 'orange');
		}	
		// Use user configured colors
		$('body').css('background-color', System.Gadget.Settings.readString('bgColor'));
		$('body').css('color', System.Gadget.Settings.readString('fgColor'));
		// Fetch the 
		var url1 = System.Gadget.Settings.readString('json1');
		var url2 = System.Gadget.Settings.readString('json2');
		var url3 = System.Gadget.Settings.readString('json3');
		var type1;
		var type2;
		var type3;
		//Convert the 'LTC ?' checkbox value to 'LTC' or 'BTC'
		if (System.Gadget.Settings.read('type1') == true) {
			type1 = 'LTC';
		} else {
			type1 = 'BTC';
		}
		if (System.Gadget.Settings.read('type2') == true) {
			type2 = 'LTC';
		} else {
			type2 = 'BTC';
		}
		if (System.Gadget.Settings.read('type3') == true) {
			type3 = 'LTC';
		} else {
			type3 = 'BTC';
		}
		
		//Hide the tables the user doesn't need
		if (count == null || count == '') {
			count = 0;
		}
		if (count == 0) {
			$('#t1').hide();
			$('#t2').hide();
			$('#t3').hide();
		} else if (count == 1) {
			$('#t1').show();
			$('#t2').hide();
			$('#t3').hide();
		} else if (count == 2) {
			$('#t1').show();
			$('#t2').show();
			$('#t3').hide();
		} else if (count == 3) {
			$('#t1').show();
			$('#t2').show();
			$('#t3').show();
		}
		
		// The return values
		var r1;
		var r2;
		var r3;
		// Number of workers: more workers = bigger gadget
		var workers = 0;
		// The actual AJAX calls
		if (url1 != null && url1 != '') {
			r1 = doAjaxCall(url1, '', type1);
			workers = workers + parseInt(r1.workers);
		}
		if (url2 != null && url2 != '') {
			r2 = doAjaxCall(url2, '2', type2);
			workers = workers +  parseInt(r2.workers);
		}
		if (url3 != null && url3 != '') {
			r3 = doAjaxCall(url3, '3', type3);
			workers = workers +  parseInt(r3.workers);
		}
		// Resize (I guestimated it so might not always be correct)	
		$('body').css('height', count*110 + 25 + workers*12);
		$('#gadgetContent').css('height', count*110 + 15 + workers*12);
	} catch(err) {
		$('#info').text(err.message);	// SHIIIIII
	}	
}