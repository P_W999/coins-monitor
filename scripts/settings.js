/*
Copyright 2013 - P_W999
	Licensed under The Code Project Open License (CPOL) 1.02 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

       http://www.codeproject.com/info/cpol10.aspx
	   
	Author: Phillip (p_w999)
	Blog: http://pw999.wordpress.com
	Contact: p_w999@ymail.com
	Twitter: @P_W999
	
	Donations are always welcome:
	- BTC: 1D1aGcfQNLS3D6bxghSVoKBnAq3pQqW8FW
	- LTC: LKu6G3BBmuu13ZiLwrbLxh7dwiJMFF9np7
*/
//Attach function to closing event
System.Gadget.onSettingsClosing	= function(event) 
{ 
	if (event.closeAction == event.Action.commit) 
	{
		//Just save everything
		System.Gadget.Settings.writeString("json1", $('#json1').val());
		System.Gadget.Settings.writeString("type1", $('#type1').prop('checked'));
		System.Gadget.Settings.writeString("json2", $('#json2').val());
		System.Gadget.Settings.writeString("type2", $('#type2').prop('checked'));
		System.Gadget.Settings.writeString("json3", $('#json3').val());
		System.Gadget.Settings.writeString("type3", $('#type3').prop('checked'));
		System.Gadget.Settings.writeString("bgColor", $('#bgColor').val());
		System.Gadget.Settings.writeString("fgColor", $('#fgColor').val());
		//Make pool count here
		var count = 0;
		if ($('#json1').val() != '') {
			count++;
		}
		if ($('#json2').val() != '') {
			count++;
		}
		if ($('#json3').val() != '') {
			count++;
		}
		System.Gadget.Settings.write("count", count);
		//Close
		event.cancel = false;
	}
}

/**
*	Fills in the fields with value that were previously saved.
*/
function setup() {
	$('#json1').val(System.Gadget.Settings.readString('json1'));
	$('#type1').prop('checked', System.Gadget.Settings.read('type1'));
	$('#json2').val(System.Gadget.Settings.readString('json2'));
	$('#type2').prop('checked', System.Gadget.Settings.read('type2'));
	$('#json3').val(System.Gadget.Settings.readString('json3'));
	$('#type3').prop('checked', System.Gadget.Settings.read('type3'));
	$('#bgColor').val(System.Gadget.Settings.readString('bgColor'));
	$('#fgColor').val(System.Gadget.Settings.readString('fgColor'));
}